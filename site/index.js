import init_wasm, { IDPWMemoWasm, get_value_type_name } from "./idpwmemo_wasm.js";

const messageLabel = document.getElementById("msg");
const detailsTable = document.querySelector("#details_table > tbody");
const secretsTable = document.querySelector("#secrets_table > tbody");
const serviceList = document.getElementById("service_list");
const showButton = document.getElementById("show");
const downloadButton = document.getElementById("download");
const downloadLink = document.getElementById("download_link");
const addNewServiceButton = document.getElementById("add_new_service");
const addNewDetailButton = document.getElementById("add_new_detail");
const addNewSecretButton = document.getElementById("add_new_secret");
const newDetailType = document.getElementById("detail_type");
const newDetailValue = document.getElementById("detail_value");
const newSecretType = document.getElementById("secret_type");
const newSecretValue = document.getElementById("secret_value");

let memo = null;

function msg(s) {
    messageLabel.textContent = `${s}`;
}

function removeAllChild(el) {
    while (el.childElementCount > 0) {
        el.lastElementChild.remove();
    }
}

function clearValues() {
    removeAllChild(detailsTable);
    removeAllChild(secretsTable);
    addNewDetailButton.disabled = true;
    addNewSecretButton.disabled = true;
    newDetailType.disabled = true;
    newDetailValue.disabled = true;
    newSecretType.disabled = true;
    newSecretValue.disabled = true;
}

function clearAll() {
    clearValues();
    removeAllChild(serviceList);
    removeAllChild(downloadLink);
    downloadButton.disabled = true;
    showButton.disabled = true;
    addNewServiceButton.disabled = true;
}

function appendSevice(index, name) {
    const opt = serviceList.appendChild(document.createElement("option"));
    opt.value = `${index}`;
    opt.textContent = `${name}`;
    return opt;
}

function appendValue(table, v) {
    const tr = table.appendChild(document.createElement("tr"));
    const typeTd = tr.appendChild(document.createElement("td"));
    const valueTd = tr.appendChild(document.createElement("td"));
    typeTd.textContent = v.get_type_name();
    valueTd.textContent = v.get_value();
}

function loadMemo(src, pw)  {
    const pb = new Uint8Array(new TextEncoder().encode(pw).buffer);
    let res = memo.set_password(pb);
    if (!res.ok) {
        throw res.get_message();
    }
    res = memo.load_memo(new Uint8Array(src));
    if (!res.ok) {
        throw res.get_message();
    }
    const names = memo.get_service_names();
    for (let i = 0; i < names.length(); i++) {
        appendSevice(i, names.get(i));
    }
    doneIt();
}

function loadSample() {
    fetch('./sample.memo')
    .then( res => res.blob() )
    .then( blob => blob.arrayBuffer() )
    .then( src => loadMemo(src, "さんぷるdayo") )
    .catch( err => msg(err) );
}

function loadMemoFile() {
    const f = document.getElementById("file_memo_file");
    if (f.files === null || f.files.length !== 1) {
        msg("file is nothing");
        return;
    }
    f.files[0].arrayBuffer()
    .then( src => {
        const pw = document.getElementById("file_memo_password").value;
        loadMemo(src, pw);
    })
    .catch( err => msg(err) );
}

function showLastUpdateTime(unixTime) {
    if (unixTime === 0n) {
        msg("LAST UPDATE: ?");
    } else {
        const d = new Date(Number(unixTime));
        msg(`LAST UPDATE: ${d.toLocaleString()}`);
    }
}

function showService() {
    msg("");
    clearValues();
    const index = parseInt(serviceList.value);
    const res = memo.select_service(index);
    if (!res.ok) {
        msg(res.get_message());
        return;
    }
    const unixTime = memo.get_last_update_date();
    if (!unixTime.ok) {
        msg(unixTime.get_message());
        return;
    }
    showLastUpdateTime(unixTime.get());
    let list = memo.get_values();
    if (!list.ok) {
        msg(list.get_message());
        return;
    }
    for (let i = 0; i < list.length(); i++) {
        appendValue(detailsTable, list.get(i));
    }
    list = memo.get_secrets();
    if (!list.ok) {
        msg(list.get_message());
        return;
    }
    for (let i = 0; i < list.length(); i++) {
        appendValue(secretsTable, list.get(i));
    }
    addNewDetailButton.disabled = false;
    addNewSecretButton.disabled = false;
    newDetailType.disabled = false;
    newDetailValue.disabled = false;
    newSecretType.disabled = false;
    newSecretValue.disabled = false;
}

function createNewMemo() {
    const pw = document.getElementById("new_memo_password").value;
    const pb = new Uint8Array(new TextEncoder().encode(pw).buffer);
    let res = memo.set_password(pb);
    if (!res.ok) {
        throw res.get_message();
    }
    res = memo.new_memo();
    if (!res.ok) {
        throw res.get_message();
    }
    doneIt();
}

function doneIt() {
    showButton.disabled = false;
    downloadButton.disabled = false;
    addNewServiceButton.disabled = false;
    msg("done");
}

function doIt() {
    msg("");
    clearAll();
    if (document.getElementById("sample_memo").checked) {
        loadSample();
    } else if (document.getElementById("file_memo").checked) {
        loadMemoFile();
    } else if (document.getElementById("new_memo").checked) {
        createNewMemo();
    }
}

function makeDownloadLink() {
    msg("");
    const res = memo.save();
    if (!res.ok) {
        msg(res.get_message());
        return;
    }
    removeAllChild(downloadLink);
    const fileName = "download.memo";
    const reader = new FileReader();
    reader.addEventListener("load", () => {
        const url = reader.result;
        if (typeof url !== "string") {
            msg("failed generating download");
            return;
        }
        const span = downloadLink;
        const a = span.appendChild(document.createElement("a"));
        a.href = url;
        a.download = fileName;
        a.textContent = fileName;
        span.appendChild(document.createElement("span"))
            .textContent = `[ ${new Date().toLocaleString()} ]`;
    });
    reader.readAsDataURL(new File([res.get().buffer], fileName));
}

function addNewService() {
    msg("");
    clearValues();
    const name = (window.prompt("NEW SERVICE NAME?", '') ?? '').trim();
    if (name === '') {
        return;
    }
    const res = memo.add_new_service(name);
    if (!res.ok) {
        msg(res.get_message());
        return;
    }
    const opt = appendSevice(res.get(), name);
    serviceList.value = opt.value;
    showService();
}

function addNewDetail() {
    msg("");
    const valueType = parseInt(newDetailType.value);
    const value = newDetailValue.value;
    const res = memo.add_new_value(valueType, value);
    if (!res.ok) {
        msg(res.get_message());
        return;
    }
    appendValue(detailsTable, res.get());
}

function addNewSecret() {
    msg("");
    const valueType = parseInt(newSecretType.value);
    const value = newSecretValue.value;
    const res = memo.add_new_secret(valueType, value);
    if (!res.ok) {
        msg(res.get_message());
        return;
    }
    appendValue(secretsTable, res.get());
}

init_wasm().then( () => {
    msg("Ok");
    memo = IDPWMemoWasm.new_instance();
    const doitButton = document.getElementById("doit");
    doitButton.addEventListener("click", doIt);
    doitButton.disabled = false;
    showButton.addEventListener("click", showService);
    downloadButton.addEventListener("click", makeDownloadLink);
    addNewServiceButton.addEventListener("click", addNewService);
    addNewDetailButton.addEventListener("click", addNewDetail);
    addNewSecretButton.addEventListener("click", addNewSecret);
    for (let i = 0; i < 8; i++) {
        const s = get_value_type_name(i);
        const opt1 = newDetailType.appendChild(document.createElement("option"));
        opt1.value = `${i}`;
        opt1.textContent = s;
        const opt2 = newSecretType.appendChild(document.createElement("option"));
        opt2.value = `${i}`;
        opt2.textContent = s;
    }
});
