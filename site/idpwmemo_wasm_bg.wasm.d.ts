/* tslint:disable */
/* eslint-disable */
export const memory: WebAssembly.Memory;
export function __wbg_idpwmemowasm_free(a: number): void;
export function __wbg_value_free(a: number): void;
export function get_value_type_name(a: number, b: number): void;
export function value_get_type_name(a: number, b: number): void;
export function value_get_type(a: number): number;
export function value_get_value(a: number, b: number): void;
export function idpwmemowasm_new_instance(): number;
export function idpwmemowasm_set_password(a: number, b: number, c: number): number;
export function idpwmemowasm_new_memo(a: number): number;
export function idpwmemowasm_load_memo(a: number, b: number, c: number): number;
export function idpwmemowasm_get_service_names(a: number): number;
export function idpwmemowasm_select_service(a: number, b: number): number;
export function idpwmemowasm_get_last_update_date(a: number): number;
export function idpwmemowasm_get_values(a: number): number;
export function idpwmemowasm_get_secrets(a: number): number;
export function idpwmemowasm_save(a: number): number;
export function idpwmemowasm_add_new_service(a: number, b: number, c: number): number;
export function idpwmemowasm_add_new_value(a: number, b: number, c: number, d: number): number;
export function idpwmemowasm_replace_value(a: number, b: number, c: number, d: number, e: number): number;
export function idpwmemowasm_add_new_secret(a: number, b: number, c: number, d: number): number;
export function idpwmemowasm_replace_secret(a: number, b: number, c: number, d: number, e: number): number;
export function idpwmemowasm_change_password(a: number, b: number, c: number): number;
export function __wbg_uint8clampedarrayresult_free(a: number): void;
export function __wbg_get_uint8clampedarrayresult_ok(a: number): number;
export function __wbg_set_uint8clampedarrayresult_ok(a: number, b: number): void;
export function uint8clampedarrayresult_get_message(a: number, b: number): void;
export function uint8clampedarrayresult_get(a: number, b: number): void;
export function __wbg_simpleresult_free(a: number): void;
export function __wbg_get_simpleresult_ok(a: number): number;
export function __wbg_set_simpleresult_ok(a: number, b: number): void;
export function simpleresult_get_message(a: number, b: number): void;
export function __wbg_valueresult_free(a: number): void;
export function __wbg_get_valueresult_ok(a: number): number;
export function __wbg_set_valueresult_ok(a: number, b: number): void;
export function valueresult_get_message(a: number, b: number): void;
export function valueresult_get(a: number): number;
export function __wbg_i64result_free(a: number): void;
export function __wbg_get_i64result_ok(a: number): number;
export function __wbg_set_i64result_ok(a: number, b: number): void;
export function __wbg_get_i64result_data(a: number, b: number): void;
export function __wbg_set_i64result_data(a: number, b: number, c: number): void;
export function i64result_get_message(a: number, b: number): void;
export function i64result_get(a: number, b: number): void;
export function __wbg_i32result_free(a: number): void;
export function __wbg_get_i32result_ok(a: number): number;
export function __wbg_set_i32result_ok(a: number, b: number): void;
export function __wbg_get_i32result_data(a: number): number;
export function __wbg_set_i32result_data(a: number, b: number): void;
export function i32result_get_message(a: number, b: number): void;
export function i32result_get(a: number): number;
export function __wbg_stringlistresult_free(a: number): void;
export function __wbg_get_stringlistresult_ok(a: number): number;
export function __wbg_set_stringlistresult_ok(a: number, b: number): void;
export function stringlistresult_get_message(a: number, b: number): void;
export function stringlistresult_length(a: number): number;
export function stringlistresult_get(a: number, b: number, c: number): void;
export function __wbg_valuelistresult_free(a: number): void;
export function __wbg_get_valuelistresult_ok(a: number): number;
export function __wbg_set_valuelistresult_ok(a: number, b: number): void;
export function valuelistresult_get_message(a: number, b: number): void;
export function valuelistresult_length(a: number): number;
export function valuelistresult_get(a: number, b: number): number;
export function __wbindgen_add_to_stack_pointer(a: number): number;
export function __wbindgen_free(a: number, b: number): void;
export function __wbindgen_malloc(a: number): number;
export function __wbindgen_realloc(a: number, b: number, c: number): number;
