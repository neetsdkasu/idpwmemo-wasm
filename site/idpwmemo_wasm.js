
let wasm;

const heap = new Array(32).fill(undefined);

heap.push(undefined, null, true, false);

function getObject(idx) { return heap[idx]; }

let heap_next = heap.length;

function dropObject(idx) {
    if (idx < 36) return;
    heap[idx] = heap_next;
    heap_next = idx;
}

function takeObject(idx) {
    const ret = getObject(idx);
    dropObject(idx);
    return ret;
}

let cachedTextDecoder = new TextDecoder('utf-8', { ignoreBOM: true, fatal: true });

cachedTextDecoder.decode();

let cachegetUint8Memory0 = null;
function getUint8Memory0() {
    if (cachegetUint8Memory0 === null || cachegetUint8Memory0.buffer !== wasm.memory.buffer) {
        cachegetUint8Memory0 = new Uint8Array(wasm.memory.buffer);
    }
    return cachegetUint8Memory0;
}

function getStringFromWasm0(ptr, len) {
    return cachedTextDecoder.decode(getUint8Memory0().subarray(ptr, ptr + len));
}

let cachegetInt32Memory0 = null;
function getInt32Memory0() {
    if (cachegetInt32Memory0 === null || cachegetInt32Memory0.buffer !== wasm.memory.buffer) {
        cachegetInt32Memory0 = new Int32Array(wasm.memory.buffer);
    }
    return cachegetInt32Memory0;
}
/**
* @param {number} n
* @returns {string | undefined}
*/
export function get_value_type_name(n) {
    try {
        const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
        wasm.get_value_type_name(retptr, n);
        var r0 = getInt32Memory0()[retptr / 4 + 0];
        var r1 = getInt32Memory0()[retptr / 4 + 1];
        let v0;
        if (r0 !== 0) {
            v0 = getStringFromWasm0(r0, r1).slice();
            wasm.__wbindgen_free(r0, r1 * 1);
        }
        return v0;
    } finally {
        wasm.__wbindgen_add_to_stack_pointer(16);
    }
}

let WASM_VECTOR_LEN = 0;

function passArray8ToWasm0(arg, malloc) {
    const ptr = malloc(arg.length * 1);
    getUint8Memory0().set(arg, ptr / 1);
    WASM_VECTOR_LEN = arg.length;
    return ptr;
}

let cachedTextEncoder = new TextEncoder('utf-8');

const encodeString = (typeof cachedTextEncoder.encodeInto === 'function'
    ? function (arg, view) {
    return cachedTextEncoder.encodeInto(arg, view);
}
    : function (arg, view) {
    const buf = cachedTextEncoder.encode(arg);
    view.set(buf);
    return {
        read: arg.length,
        written: buf.length
    };
});

function passStringToWasm0(arg, malloc, realloc) {

    if (realloc === undefined) {
        const buf = cachedTextEncoder.encode(arg);
        const ptr = malloc(buf.length);
        getUint8Memory0().subarray(ptr, ptr + buf.length).set(buf);
        WASM_VECTOR_LEN = buf.length;
        return ptr;
    }

    let len = arg.length;
    let ptr = malloc(len);

    const mem = getUint8Memory0();

    let offset = 0;

    for (; offset < len; offset++) {
        const code = arg.charCodeAt(offset);
        if (code > 0x7F) break;
        mem[ptr + offset] = code;
    }

    if (offset !== len) {
        if (offset !== 0) {
            arg = arg.slice(offset);
        }
        ptr = realloc(ptr, len, len = offset + arg.length * 3);
        const view = getUint8Memory0().subarray(ptr + offset, ptr + len);
        const ret = encodeString(arg, view);

        offset += ret.written;
    }

    WASM_VECTOR_LEN = offset;
    return ptr;
}

let cachegetUint8ClampedMemory0 = null;
function getUint8ClampedMemory0() {
    if (cachegetUint8ClampedMemory0 === null || cachegetUint8ClampedMemory0.buffer !== wasm.memory.buffer) {
        cachegetUint8ClampedMemory0 = new Uint8ClampedArray(wasm.memory.buffer);
    }
    return cachegetUint8ClampedMemory0;
}

function getClampedArrayU8FromWasm0(ptr, len) {
    return getUint8ClampedMemory0().subarray(ptr / 1, ptr / 1 + len);
}

const u32CvtShim = new Uint32Array(2);

const int64CvtShim = new BigInt64Array(u32CvtShim.buffer);

function addHeapObject(obj) {
    if (heap_next === heap.length) heap.push(heap.length + 1);
    const idx = heap_next;
    heap_next = heap[idx];

    heap[idx] = obj;
    return idx;
}
/**
*/
export class I32Result {

    static __wrap(ptr) {
        const obj = Object.create(I32Result.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_i32result_free(ptr);
    }
    /**
    */
    get ok() {
        var ret = wasm.__wbg_get_i32result_ok(this.ptr);
        return ret !== 0;
    }
    /**
    * @param {boolean} arg0
    */
    set ok(arg0) {
        wasm.__wbg_set_i32result_ok(this.ptr, arg0);
    }
    /**
    */
    get data() {
        var ret = wasm.__wbg_get_i32result_data(this.ptr);
        return ret;
    }
    /**
    * @param {number} arg0
    */
    set data(arg0) {
        wasm.__wbg_set_i32result_data(this.ptr, arg0);
    }
    /**
    * @returns {string | undefined}
    */
    get_message() {
        try {
            const ptr = this.__destroy_into_raw();
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.i32result_get_message(retptr, ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            let v0;
            if (r0 !== 0) {
                v0 = getStringFromWasm0(r0, r1).slice();
                wasm.__wbindgen_free(r0, r1 * 1);
            }
            return v0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @returns {number}
    */
    get() {
        var ret = wasm.i32result_get(this.ptr);
        return ret;
    }
}
/**
*/
export class I64Result {

    static __wrap(ptr) {
        const obj = Object.create(I64Result.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_i64result_free(ptr);
    }
    /**
    */
    get ok() {
        var ret = wasm.__wbg_get_i64result_ok(this.ptr);
        return ret !== 0;
    }
    /**
    * @param {boolean} arg0
    */
    set ok(arg0) {
        wasm.__wbg_set_i64result_ok(this.ptr, arg0);
    }
    /**
    */
    get data() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.__wbg_get_i64result_data(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            u32CvtShim[0] = r0;
            u32CvtShim[1] = r1;
            const n0 = int64CvtShim[0];
            return n0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @param {BigInt} arg0
    */
    set data(arg0) {
        int64CvtShim[0] = arg0;
        const low0 = u32CvtShim[0];
        const high0 = u32CvtShim[1];
        wasm.__wbg_set_i64result_data(this.ptr, low0, high0);
    }
    /**
    * @returns {string | undefined}
    */
    get_message() {
        try {
            const ptr = this.__destroy_into_raw();
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.i64result_get_message(retptr, ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            let v0;
            if (r0 !== 0) {
                v0 = getStringFromWasm0(r0, r1).slice();
                wasm.__wbindgen_free(r0, r1 * 1);
            }
            return v0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @returns {BigInt}
    */
    get() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.i64result_get(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            u32CvtShim[0] = r0;
            u32CvtShim[1] = r1;
            const n0 = int64CvtShim[0];
            return n0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
}
/**
*/
export class IDPWMemoWasm {

    static __wrap(ptr) {
        const obj = Object.create(IDPWMemoWasm.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_idpwmemowasm_free(ptr);
    }
    /**
    * @returns {IDPWMemoWasm}
    */
    static new_instance() {
        var ret = wasm.idpwmemowasm_new_instance();
        return IDPWMemoWasm.__wrap(ret);
    }
    /**
    * @param {Uint8Array} password
    * @returns {SimpleResult}
    */
    set_password(password) {
        var ptr0 = passArray8ToWasm0(password, wasm.__wbindgen_malloc);
        var len0 = WASM_VECTOR_LEN;
        var ret = wasm.idpwmemowasm_set_password(this.ptr, ptr0, len0);
        return SimpleResult.__wrap(ret);
    }
    /**
    * @returns {SimpleResult}
    */
    new_memo() {
        var ret = wasm.idpwmemowasm_new_memo(this.ptr);
        return SimpleResult.__wrap(ret);
    }
    /**
    * @param {Uint8Array} src
    * @returns {SimpleResult}
    */
    load_memo(src) {
        var ptr0 = passArray8ToWasm0(src, wasm.__wbindgen_malloc);
        var len0 = WASM_VECTOR_LEN;
        var ret = wasm.idpwmemowasm_load_memo(this.ptr, ptr0, len0);
        return SimpleResult.__wrap(ret);
    }
    /**
    * @returns {StringListResult}
    */
    get_service_names() {
        var ret = wasm.idpwmemowasm_get_service_names(this.ptr);
        return StringListResult.__wrap(ret);
    }
    /**
    * @param {number} index
    * @returns {SimpleResult}
    */
    select_service(index) {
        var ret = wasm.idpwmemowasm_select_service(this.ptr, index);
        return SimpleResult.__wrap(ret);
    }
    /**
    * @returns {I64Result}
    */
    get_last_update_date() {
        var ret = wasm.idpwmemowasm_get_last_update_date(this.ptr);
        return I64Result.__wrap(ret);
    }
    /**
    * @returns {ValueListResult}
    */
    get_values() {
        var ret = wasm.idpwmemowasm_get_values(this.ptr);
        return ValueListResult.__wrap(ret);
    }
    /**
    * @returns {ValueListResult}
    */
    get_secrets() {
        var ret = wasm.idpwmemowasm_get_secrets(this.ptr);
        return ValueListResult.__wrap(ret);
    }
    /**
    * @returns {Uint8ClampedArrayResult}
    */
    save() {
        var ret = wasm.idpwmemowasm_save(this.ptr);
        return Uint8ClampedArrayResult.__wrap(ret);
    }
    /**
    * @param {string} name
    * @returns {I32Result}
    */
    add_new_service(name) {
        var ptr0 = passStringToWasm0(name, wasm.__wbindgen_malloc, wasm.__wbindgen_realloc);
        var len0 = WASM_VECTOR_LEN;
        var ret = wasm.idpwmemowasm_add_new_service(this.ptr, ptr0, len0);
        return I32Result.__wrap(ret);
    }
    /**
    * @param {number} value_type
    * @param {string} value
    * @returns {ValueResult}
    */
    add_new_value(value_type, value) {
        var ptr0 = passStringToWasm0(value, wasm.__wbindgen_malloc, wasm.__wbindgen_realloc);
        var len0 = WASM_VECTOR_LEN;
        var ret = wasm.idpwmemowasm_add_new_value(this.ptr, value_type, ptr0, len0);
        return ValueResult.__wrap(ret);
    }
    /**
    * @param {number} index
    * @param {number} value_type
    * @param {string} value
    * @returns {ValueResult}
    */
    replace_value(index, value_type, value) {
        var ptr0 = passStringToWasm0(value, wasm.__wbindgen_malloc, wasm.__wbindgen_realloc);
        var len0 = WASM_VECTOR_LEN;
        var ret = wasm.idpwmemowasm_replace_value(this.ptr, index, value_type, ptr0, len0);
        return ValueResult.__wrap(ret);
    }
    /**
    * @param {number} value_type
    * @param {string} value
    * @returns {ValueResult}
    */
    add_new_secret(value_type, value) {
        var ptr0 = passStringToWasm0(value, wasm.__wbindgen_malloc, wasm.__wbindgen_realloc);
        var len0 = WASM_VECTOR_LEN;
        var ret = wasm.idpwmemowasm_add_new_secret(this.ptr, value_type, ptr0, len0);
        return ValueResult.__wrap(ret);
    }
    /**
    * @param {number} index
    * @param {number} value_type
    * @param {string} value
    * @returns {ValueResult}
    */
    replace_secret(index, value_type, value) {
        var ptr0 = passStringToWasm0(value, wasm.__wbindgen_malloc, wasm.__wbindgen_realloc);
        var len0 = WASM_VECTOR_LEN;
        var ret = wasm.idpwmemowasm_replace_secret(this.ptr, index, value_type, ptr0, len0);
        return ValueResult.__wrap(ret);
    }
    /**
    * @param {Uint8Array} new_password
    * @returns {SimpleResult}
    */
    change_password(new_password) {
        var ptr0 = passArray8ToWasm0(new_password, wasm.__wbindgen_malloc);
        var len0 = WASM_VECTOR_LEN;
        var ret = wasm.idpwmemowasm_change_password(this.ptr, ptr0, len0);
        return SimpleResult.__wrap(ret);
    }
}
/**
*/
export class SimpleResult {

    static __wrap(ptr) {
        const obj = Object.create(SimpleResult.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_simpleresult_free(ptr);
    }
    /**
    */
    get ok() {
        var ret = wasm.__wbg_get_simpleresult_ok(this.ptr);
        return ret !== 0;
    }
    /**
    * @param {boolean} arg0
    */
    set ok(arg0) {
        wasm.__wbg_set_simpleresult_ok(this.ptr, arg0);
    }
    /**
    * @returns {string | undefined}
    */
    get_message() {
        try {
            const ptr = this.__destroy_into_raw();
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.simpleresult_get_message(retptr, ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            let v0;
            if (r0 !== 0) {
                v0 = getStringFromWasm0(r0, r1).slice();
                wasm.__wbindgen_free(r0, r1 * 1);
            }
            return v0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
}
/**
*/
export class StringListResult {

    static __wrap(ptr) {
        const obj = Object.create(StringListResult.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_stringlistresult_free(ptr);
    }
    /**
    */
    get ok() {
        var ret = wasm.__wbg_get_stringlistresult_ok(this.ptr);
        return ret !== 0;
    }
    /**
    * @param {boolean} arg0
    */
    set ok(arg0) {
        wasm.__wbg_set_stringlistresult_ok(this.ptr, arg0);
    }
    /**
    * @returns {string | undefined}
    */
    get_message() {
        try {
            const ptr = this.__destroy_into_raw();
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.stringlistresult_get_message(retptr, ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            let v0;
            if (r0 !== 0) {
                v0 = getStringFromWasm0(r0, r1).slice();
                wasm.__wbindgen_free(r0, r1 * 1);
            }
            return v0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @returns {number}
    */
    length() {
        var ret = wasm.stringlistresult_length(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} i
    * @returns {string | undefined}
    */
    get(i) {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.stringlistresult_get(retptr, this.ptr, i);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            let v0;
            if (r0 !== 0) {
                v0 = getStringFromWasm0(r0, r1).slice();
                wasm.__wbindgen_free(r0, r1 * 1);
            }
            return v0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
}
/**
*/
export class Uint8ClampedArrayResult {

    static __wrap(ptr) {
        const obj = Object.create(Uint8ClampedArrayResult.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_uint8clampedarrayresult_free(ptr);
    }
    /**
    */
    get ok() {
        var ret = wasm.__wbg_get_uint8clampedarrayresult_ok(this.ptr);
        return ret !== 0;
    }
    /**
    * @param {boolean} arg0
    */
    set ok(arg0) {
        wasm.__wbg_set_uint8clampedarrayresult_ok(this.ptr, arg0);
    }
    /**
    * @returns {string | undefined}
    */
    get_message() {
        try {
            const ptr = this.__destroy_into_raw();
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.uint8clampedarrayresult_get_message(retptr, ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            let v0;
            if (r0 !== 0) {
                v0 = getStringFromWasm0(r0, r1).slice();
                wasm.__wbindgen_free(r0, r1 * 1);
            }
            return v0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @returns {Uint8ClampedArray}
    */
    get() {
        try {
            const ptr = this.__destroy_into_raw();
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.uint8clampedarrayresult_get(retptr, ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            var v0 = getClampedArrayU8FromWasm0(r0, r1).slice();
            wasm.__wbindgen_free(r0, r1 * 1);
            return v0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
}
/**
*/
export class Value {

    static __wrap(ptr) {
        const obj = Object.create(Value.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_value_free(ptr);
    }
    /**
    * @returns {string}
    */
    get_type_name() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.value_get_type_name(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return getStringFromWasm0(r0, r1);
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
            wasm.__wbindgen_free(r0, r1);
        }
    }
    /**
    * @returns {number}
    */
    get_type() {
        var ret = wasm.value_get_type(this.ptr);
        return ret;
    }
    /**
    * @returns {string}
    */
    get_value() {
        try {
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.value_get_value(retptr, this.ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            return getStringFromWasm0(r0, r1);
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
            wasm.__wbindgen_free(r0, r1);
        }
    }
}
/**
*/
export class ValueListResult {

    static __wrap(ptr) {
        const obj = Object.create(ValueListResult.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_valuelistresult_free(ptr);
    }
    /**
    */
    get ok() {
        var ret = wasm.__wbg_get_valuelistresult_ok(this.ptr);
        return ret !== 0;
    }
    /**
    * @param {boolean} arg0
    */
    set ok(arg0) {
        wasm.__wbg_set_valuelistresult_ok(this.ptr, arg0);
    }
    /**
    * @returns {string | undefined}
    */
    get_message() {
        try {
            const ptr = this.__destroy_into_raw();
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.valuelistresult_get_message(retptr, ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            let v0;
            if (r0 !== 0) {
                v0 = getStringFromWasm0(r0, r1).slice();
                wasm.__wbindgen_free(r0, r1 * 1);
            }
            return v0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @returns {number}
    */
    length() {
        var ret = wasm.valuelistresult_length(this.ptr);
        return ret >>> 0;
    }
    /**
    * @param {number} i
    * @returns {Value | undefined}
    */
    get(i) {
        var ret = wasm.valuelistresult_get(this.ptr, i);
        return ret === 0 ? undefined : Value.__wrap(ret);
    }
}
/**
*/
export class ValueResult {

    static __wrap(ptr) {
        const obj = Object.create(ValueResult.prototype);
        obj.ptr = ptr;

        return obj;
    }

    __destroy_into_raw() {
        const ptr = this.ptr;
        this.ptr = 0;

        return ptr;
    }

    free() {
        const ptr = this.__destroy_into_raw();
        wasm.__wbg_valueresult_free(ptr);
    }
    /**
    */
    get ok() {
        var ret = wasm.__wbg_get_valueresult_ok(this.ptr);
        return ret !== 0;
    }
    /**
    * @param {boolean} arg0
    */
    set ok(arg0) {
        wasm.__wbg_set_valueresult_ok(this.ptr, arg0);
    }
    /**
    * @returns {string | undefined}
    */
    get_message() {
        try {
            const ptr = this.__destroy_into_raw();
            const retptr = wasm.__wbindgen_add_to_stack_pointer(-16);
            wasm.valueresult_get_message(retptr, ptr);
            var r0 = getInt32Memory0()[retptr / 4 + 0];
            var r1 = getInt32Memory0()[retptr / 4 + 1];
            let v0;
            if (r0 !== 0) {
                v0 = getStringFromWasm0(r0, r1).slice();
                wasm.__wbindgen_free(r0, r1 * 1);
            }
            return v0;
        } finally {
            wasm.__wbindgen_add_to_stack_pointer(16);
        }
    }
    /**
    * @returns {Value | undefined}
    */
    get() {
        const ptr = this.__destroy_into_raw();
        var ret = wasm.valueresult_get(ptr);
        return ret === 0 ? undefined : Value.__wrap(ret);
    }
}

async function load(module, imports) {
    if (typeof Response === 'function' && module instanceof Response) {
        if (typeof WebAssembly.instantiateStreaming === 'function') {
            try {
                return await WebAssembly.instantiateStreaming(module, imports);

            } catch (e) {
                if (module.headers.get('Content-Type') != 'application/wasm') {
                    console.warn("`WebAssembly.instantiateStreaming` failed because your server does not serve wasm with `application/wasm` MIME type. Falling back to `WebAssembly.instantiate` which is slower. Original error:\n", e);

                } else {
                    throw e;
                }
            }
        }

        const bytes = await module.arrayBuffer();
        return await WebAssembly.instantiate(bytes, imports);

    } else {
        const instance = await WebAssembly.instantiate(module, imports);

        if (instance instanceof WebAssembly.Instance) {
            return { instance, module };

        } else {
            return instance;
        }
    }
}

async function init(input) {
    if (typeof input === 'undefined') {
        input = new URL('idpwmemo_wasm_bg.wasm', import.meta.url);
    }
    const imports = {};
    imports.wbg = {};
    imports.wbg.__wbg_new_ac0ec09ce39c7be2 = function() {
        var ret = new Error();
        return addHeapObject(ret);
    };
    imports.wbg.__wbg_stack_a47b34ca8d218cac = function(arg0, arg1) {
        var ret = getObject(arg1).stack;
        var ptr0 = passStringToWasm0(ret, wasm.__wbindgen_malloc, wasm.__wbindgen_realloc);
        var len0 = WASM_VECTOR_LEN;
        getInt32Memory0()[arg0 / 4 + 1] = len0;
        getInt32Memory0()[arg0 / 4 + 0] = ptr0;
    };
    imports.wbg.__wbg_error_31538d7309e9dc6d = function(arg0, arg1) {
        try {
            console.error(getStringFromWasm0(arg0, arg1));
        } finally {
            wasm.__wbindgen_free(arg0, arg1);
        }
    };
    imports.wbg.__wbindgen_object_drop_ref = function(arg0) {
        takeObject(arg0);
    };
    imports.wbg.__wbindgen_throw = function(arg0, arg1) {
        throw new Error(getStringFromWasm0(arg0, arg1));
    };

    if (typeof input === 'string' || (typeof Request === 'function' && input instanceof Request) || (typeof URL === 'function' && input instanceof URL)) {
        input = fetch(input);
    }



    const { instance, module } = await load(await input, imports);

    wasm = instance.exports;
    init.__wbindgen_wasm_module = module;

    return wasm;
}

export default init;

