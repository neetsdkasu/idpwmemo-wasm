/* tslint:disable */
/* eslint-disable */
/**
* @param {number} n
* @returns {string | undefined}
*/
export function get_value_type_name(n: number): string | undefined;
/**
*/
export class I32Result {
  free(): void;
/**
* @returns {string | undefined}
*/
  get_message(): string | undefined;
/**
* @returns {number}
*/
  get(): number;
/**
*/
  data: number;
/**
*/
  ok: boolean;
}
/**
*/
export class I64Result {
  free(): void;
/**
* @returns {string | undefined}
*/
  get_message(): string | undefined;
/**
* @returns {BigInt}
*/
  get(): BigInt;
/**
*/
  data: BigInt;
/**
*/
  ok: boolean;
}
/**
*/
export class IDPWMemoWasm {
  free(): void;
/**
* @returns {IDPWMemoWasm}
*/
  static new_instance(): IDPWMemoWasm;
/**
* @param {Uint8Array} password
* @returns {SimpleResult}
*/
  set_password(password: Uint8Array): SimpleResult;
/**
* @returns {SimpleResult}
*/
  new_memo(): SimpleResult;
/**
* @param {Uint8Array} src
* @returns {SimpleResult}
*/
  load_memo(src: Uint8Array): SimpleResult;
/**
* @returns {StringListResult}
*/
  get_service_names(): StringListResult;
/**
* @param {number} index
* @returns {SimpleResult}
*/
  select_service(index: number): SimpleResult;
/**
* @returns {I64Result}
*/
  get_last_update_date(): I64Result;
/**
* @returns {ValueListResult}
*/
  get_values(): ValueListResult;
/**
* @returns {ValueListResult}
*/
  get_secrets(): ValueListResult;
/**
* @returns {Uint8ClampedArrayResult}
*/
  save(): Uint8ClampedArrayResult;
/**
* @param {string} name
* @returns {I32Result}
*/
  add_new_service(name: string): I32Result;
/**
* @param {number} value_type
* @param {string} value
* @returns {ValueResult}
*/
  add_new_value(value_type: number, value: string): ValueResult;
/**
* @param {number} index
* @param {number} value_type
* @param {string} value
* @returns {ValueResult}
*/
  replace_value(index: number, value_type: number, value: string): ValueResult;
/**
* @param {number} value_type
* @param {string} value
* @returns {ValueResult}
*/
  add_new_secret(value_type: number, value: string): ValueResult;
/**
* @param {number} index
* @param {number} value_type
* @param {string} value
* @returns {ValueResult}
*/
  replace_secret(index: number, value_type: number, value: string): ValueResult;
/**
* @param {Uint8Array} new_password
* @returns {SimpleResult}
*/
  change_password(new_password: Uint8Array): SimpleResult;
}
/**
*/
export class SimpleResult {
  free(): void;
/**
* @returns {string | undefined}
*/
  get_message(): string | undefined;
/**
*/
  ok: boolean;
}
/**
*/
export class StringListResult {
  free(): void;
/**
* @returns {string | undefined}
*/
  get_message(): string | undefined;
/**
* @returns {number}
*/
  length(): number;
/**
* @param {number} i
* @returns {string | undefined}
*/
  get(i: number): string | undefined;
/**
*/
  ok: boolean;
}
/**
*/
export class Uint8ClampedArrayResult {
  free(): void;
/**
* @returns {string | undefined}
*/
  get_message(): string | undefined;
/**
* @returns {Uint8ClampedArray}
*/
  get(): Uint8ClampedArray;
/**
*/
  ok: boolean;
}
/**
*/
export class Value {
  free(): void;
/**
* @returns {string}
*/
  get_type_name(): string;
/**
* @returns {number}
*/
  get_type(): number;
/**
* @returns {string}
*/
  get_value(): string;
}
/**
*/
export class ValueListResult {
  free(): void;
/**
* @returns {string | undefined}
*/
  get_message(): string | undefined;
/**
* @returns {number}
*/
  length(): number;
/**
* @param {number} i
* @returns {Value | undefined}
*/
  get(i: number): Value | undefined;
/**
*/
  ok: boolean;
}
/**
*/
export class ValueResult {
  free(): void;
/**
* @returns {string | undefined}
*/
  get_message(): string | undefined;
/**
* @returns {Value | undefined}
*/
  get(): Value | undefined;
/**
*/
  ok: boolean;
}

export type InitInput = RequestInfo | URL | Response | BufferSource | WebAssembly.Module;

export interface InitOutput {
  readonly memory: WebAssembly.Memory;
  readonly __wbg_idpwmemowasm_free: (a: number) => void;
  readonly __wbg_value_free: (a: number) => void;
  readonly get_value_type_name: (a: number, b: number) => void;
  readonly value_get_type_name: (a: number, b: number) => void;
  readonly value_get_type: (a: number) => number;
  readonly value_get_value: (a: number, b: number) => void;
  readonly idpwmemowasm_new_instance: () => number;
  readonly idpwmemowasm_set_password: (a: number, b: number, c: number) => number;
  readonly idpwmemowasm_new_memo: (a: number) => number;
  readonly idpwmemowasm_load_memo: (a: number, b: number, c: number) => number;
  readonly idpwmemowasm_get_service_names: (a: number) => number;
  readonly idpwmemowasm_select_service: (a: number, b: number) => number;
  readonly idpwmemowasm_get_last_update_date: (a: number) => number;
  readonly idpwmemowasm_get_values: (a: number) => number;
  readonly idpwmemowasm_get_secrets: (a: number) => number;
  readonly idpwmemowasm_save: (a: number) => number;
  readonly idpwmemowasm_add_new_service: (a: number, b: number, c: number) => number;
  readonly idpwmemowasm_add_new_value: (a: number, b: number, c: number, d: number) => number;
  readonly idpwmemowasm_replace_value: (a: number, b: number, c: number, d: number, e: number) => number;
  readonly idpwmemowasm_add_new_secret: (a: number, b: number, c: number, d: number) => number;
  readonly idpwmemowasm_replace_secret: (a: number, b: number, c: number, d: number, e: number) => number;
  readonly idpwmemowasm_change_password: (a: number, b: number, c: number) => number;
  readonly __wbg_uint8clampedarrayresult_free: (a: number) => void;
  readonly __wbg_get_uint8clampedarrayresult_ok: (a: number) => number;
  readonly __wbg_set_uint8clampedarrayresult_ok: (a: number, b: number) => void;
  readonly uint8clampedarrayresult_get_message: (a: number, b: number) => void;
  readonly uint8clampedarrayresult_get: (a: number, b: number) => void;
  readonly __wbg_simpleresult_free: (a: number) => void;
  readonly __wbg_get_simpleresult_ok: (a: number) => number;
  readonly __wbg_set_simpleresult_ok: (a: number, b: number) => void;
  readonly simpleresult_get_message: (a: number, b: number) => void;
  readonly __wbg_valueresult_free: (a: number) => void;
  readonly __wbg_get_valueresult_ok: (a: number) => number;
  readonly __wbg_set_valueresult_ok: (a: number, b: number) => void;
  readonly valueresult_get_message: (a: number, b: number) => void;
  readonly valueresult_get: (a: number) => number;
  readonly __wbg_i64result_free: (a: number) => void;
  readonly __wbg_get_i64result_ok: (a: number) => number;
  readonly __wbg_set_i64result_ok: (a: number, b: number) => void;
  readonly __wbg_get_i64result_data: (a: number, b: number) => void;
  readonly __wbg_set_i64result_data: (a: number, b: number, c: number) => void;
  readonly i64result_get_message: (a: number, b: number) => void;
  readonly i64result_get: (a: number, b: number) => void;
  readonly __wbg_i32result_free: (a: number) => void;
  readonly __wbg_get_i32result_ok: (a: number) => number;
  readonly __wbg_set_i32result_ok: (a: number, b: number) => void;
  readonly __wbg_get_i32result_data: (a: number) => number;
  readonly __wbg_set_i32result_data: (a: number, b: number) => void;
  readonly i32result_get_message: (a: number, b: number) => void;
  readonly i32result_get: (a: number) => number;
  readonly __wbg_stringlistresult_free: (a: number) => void;
  readonly __wbg_get_stringlistresult_ok: (a: number) => number;
  readonly __wbg_set_stringlistresult_ok: (a: number, b: number) => void;
  readonly stringlistresult_get_message: (a: number, b: number) => void;
  readonly stringlistresult_length: (a: number) => number;
  readonly stringlistresult_get: (a: number, b: number, c: number) => void;
  readonly __wbg_valuelistresult_free: (a: number) => void;
  readonly __wbg_get_valuelistresult_ok: (a: number) => number;
  readonly __wbg_set_valuelistresult_ok: (a: number, b: number) => void;
  readonly valuelistresult_get_message: (a: number, b: number) => void;
  readonly valuelistresult_length: (a: number) => number;
  readonly valuelistresult_get: (a: number, b: number) => number;
  readonly __wbindgen_add_to_stack_pointer: (a: number) => number;
  readonly __wbindgen_free: (a: number, b: number) => void;
  readonly __wbindgen_malloc: (a: number) => number;
  readonly __wbindgen_realloc: (a: number, b: number, c: number) => number;
}

/**
* If `module_or_path` is {RequestInfo} or {URL}, makes a request and
* for everything else, calls `WebAssembly.instantiate` directly.
*
* @param {InitInput | Promise<InitInput>} module_or_path
*
* @returns {Promise<InitOutput>}
*/
export default function init (module_or_path?: InitInput | Promise<InitInput>): Promise<InitOutput>;
