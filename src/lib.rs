mod utils;

use wasm_bindgen::prelude::*;
use wasm_bindgen::Clamped;

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
pub struct IDPWMemoWasm {
    inner: idpwmemo_rs::IDPWMemo,
}

#[wasm_bindgen]
#[derive(Clone)]
pub struct Value {
    inner: idpwmemo_rs::Value,
}

#[wasm_bindgen]
pub fn get_value_type_name(n: i32) -> Option<String> {
    use std::convert::TryFrom;
    match idpwmemo_rs::ValueType::try_from(n) {
        Ok(t) => Some(t.to_string()),
        Err(_) => None,
    }
}

#[wasm_bindgen]
impl Value {
    pub fn get_type_name(&self) -> String {
        self.inner.value_type.to_string()
    }
    pub fn get_type(&self) -> i32 {
        self.inner.value_type as i32
    }
    pub fn get_value(&self) -> String {
        self.inner.value.clone()
    }
}

#[wasm_bindgen]
impl IDPWMemoWasm {
    pub fn new_instance() -> Self {
        utils::set_panic_hook();
        Self {
            inner: Default::default(),
        }
    }

    pub fn set_password(&mut self, password: &[u8]) -> SimpleResult {
        match self.inner.set_password(password) {
            Ok(_) => SimpleResult::success(),
            Err(e) => SimpleResult::failed(e.to_string()),
        }
    }

    pub fn new_memo(&mut self) -> SimpleResult {
        match self.inner.new_memo() {
            Ok(_) => SimpleResult::success(),
            Err(e) => SimpleResult::failed(e.to_string()),
        }
    }

    pub fn load_memo(&mut self, src: &[u8]) -> SimpleResult {
        match self.inner.load_memo(src) {
            Ok(_) => SimpleResult::success(),
            Err(e) => SimpleResult::failed(e.to_string()),
        }
    }

    pub fn get_service_names(&self) -> StringListResult {
        match self.inner.get_service_names() {
            Ok(names) => {
                let mut data: Vec<String> = vec![];
                for s in names.iter() {
                    data.push(s.to_string());
                }
                StringListResult::success(data)
            }
            Err(e) => StringListResult::failed(e.to_string()),
        }
    }

    pub fn select_service(&mut self, index: usize) -> SimpleResult {
        match self.inner.select_service(index) {
            Ok(_) => SimpleResult::success(),
            Err(e) => SimpleResult::failed(e.to_string()),
        }
    }

    pub fn get_last_update_date(&self) -> I64Result {
        match self.inner.service() {
            Ok(s) => I64Result::success(s.time),
            Err(e) => I64Result::failed(e.to_string()),
        }
    }

    pub fn get_values(&self) -> ValueListResult {
        match self.inner.values() {
            Ok(list) => {
                let mut data: Vec<Value> = vec![];
                for v in list.iter() {
                    data.push(Value { inner: v.clone() });
                }
                ValueListResult::success(data)
            }
            Err(e) => ValueListResult::failed(e.to_string()),
        }
    }

    pub fn get_secrets(&mut self) -> ValueListResult {
        match self.inner.secrets() {
            Ok(list) => {
                let mut data: Vec<Value> = vec![];
                for v in list.iter() {
                    data.push(Value { inner: v.clone() });
                }
                ValueListResult::success(data)
            }
            Err(e) => ValueListResult::failed(e.to_string()),
        }
    }

    pub fn save(&mut self) -> Uint8ClampedArrayResult {
        match self.inner.save() {
            Ok(data) => Uint8ClampedArrayResult::success(data),
            Err(e) => Uint8ClampedArrayResult::failed(e.to_string()),
        }
    }

    pub fn add_new_service(&mut self, name: &str) -> I32Result {
        match self
            .inner
            .add_new_service(name)
            .and_then(|_| self.inner.services())
            .map(|s| s.len())
        {
            Ok(len) => I32Result::success(len as i32 - 1),
            Err(e) => I32Result::failed(e.to_string()),
        }
    }

    pub fn add_new_value(&mut self, value_type: i32, value: &str) -> ValueResult {
        use std::convert::TryFrom;
        let value_type = match idpwmemo_rs::ValueType::try_from(value_type) {
            Ok(value_type) => value_type,
            Err(_) => return ValueResult::failed("Invalid ValueType".to_string()),
        };
        match self.inner.values_mut() {
            Ok(values) => {
                let value = idpwmemo_rs::Value::new(value_type, value);
                values.push(value.clone());
                ValueResult::success(Value { inner: value })
            }
            Err(e) => ValueResult::failed(e.to_string()),
        }
    }

    pub fn replace_value(&mut self, index: usize, value_type: i32, value: &str) -> ValueResult {
        use std::convert::TryFrom;
        let value_type = match idpwmemo_rs::ValueType::try_from(value_type) {
            Ok(value_type) => value_type,
            Err(_) => return ValueResult::failed("Invalid ValueType".to_string()),
        };
        let values = match self.inner.values_mut() {
            Ok(values) => values,
            Err(e) => return ValueResult::failed(e.to_string()),
        };
        match values.get_mut(index) {
            Some(v) => {
                v.value_type = value_type;
                v.value = value.to_string();
                ValueResult::success(Value { inner: v.clone() })
            }
            None => ValueResult::failed("Invalid Index".to_string()),
        }
    }

    pub fn add_new_secret(&mut self, value_type: i32, value: &str) -> ValueResult {
        use std::convert::TryFrom;
        let value_type = match idpwmemo_rs::ValueType::try_from(value_type) {
            Ok(value_type) => value_type,
            Err(_) => return ValueResult::failed("Invalid ValueType".to_string()),
        };
        match self.inner.secrets_mut() {
            Ok(secrets) => {
                let value = idpwmemo_rs::Value::new(value_type, value);
                secrets.push(value.clone());
                ValueResult::success(Value { inner: value })
            }
            Err(e) => ValueResult::failed(e.to_string()),
        }
    }

    pub fn replace_secret(&mut self, index: usize, value_type: i32, value: &str) -> ValueResult {
        use std::convert::TryFrom;
        let value_type = match idpwmemo_rs::ValueType::try_from(value_type) {
            Ok(value_type) => value_type,
            Err(_) => return ValueResult::failed("Invalid ValueType".to_string()),
        };
        let values = match self.inner.secrets_mut() {
            Ok(values) => values,
            Err(e) => return ValueResult::failed(e.to_string()),
        };
        match values.get_mut(index) {
            Some(v) => {
                v.value_type = value_type;
                v.value = value.to_string();
                ValueResult::success(Value { inner: v.clone() })
            }
            None => ValueResult::failed("Invalid Index".to_string()),
        }
    }

    pub fn change_password(&mut self, new_password: &[u8]) -> SimpleResult {
        match self.inner.change_password(new_password) {
            Ok(_) => SimpleResult::success(),
            Err(e) => SimpleResult::failed(e.to_string()),
        }
    }
}

#[wasm_bindgen]
pub struct Uint8ClampedArrayResult {
    pub ok: bool,
    data: Vec<u8>,
    err_msg: Option<String>,
}

#[wasm_bindgen]
impl Uint8ClampedArrayResult {
    pub fn get_message(self) -> Option<String> {
        self.err_msg
    }
    pub fn get(self) -> Clamped<Vec<u8>> {
        Clamped(self.data)
    }
    fn success(data: Vec<u8>) -> Self {
        Self {
            ok: true,
            data,
            err_msg: None,
        }
    }
    fn failed(msg: String) -> Self {
        Self {
            ok: false,
            data: vec![],
            err_msg: Some(msg),
        }
    }
}

#[wasm_bindgen]
pub struct SimpleResult {
    pub ok: bool,
    err_msg: Option<String>,
}

#[wasm_bindgen]
impl SimpleResult {
    pub fn get_message(self) -> Option<String> {
        self.err_msg
    }
    fn success() -> Self {
        Self {
            ok: true,
            err_msg: None,
        }
    }
    fn failed(msg: String) -> Self {
        Self {
            ok: false,
            err_msg: Some(msg),
        }
    }
}

#[wasm_bindgen]
pub struct ValueResult {
    pub ok: bool,
    data: Option<Value>,
    err_msg: Option<String>,
}

#[wasm_bindgen]
impl ValueResult {
    pub fn get_message(self) -> Option<String> {
        self.err_msg
    }
    pub fn get(self) -> Option<Value> {
        self.data
    }
    fn success(data: Value) -> Self {
        Self {
            ok: true,
            data: Some(data),
            err_msg: None,
        }
    }
    fn failed(msg: String) -> Self {
        Self {
            ok: false,
            data: None,
            err_msg: Some(msg),
        }
    }
}

#[wasm_bindgen]
pub struct I64Result {
    pub ok: bool,
    pub data: i64,
    err_msg: Option<String>,
}

#[wasm_bindgen]
impl I64Result {
    pub fn get_message(self) -> Option<String> {
        self.err_msg
    }
    pub fn get(&self) -> i64 {
        self.data
    }
    fn success(data: i64) -> Self {
        Self {
            ok: true,
            data,
            err_msg: None,
        }
    }
    fn failed(msg: String) -> Self {
        Self {
            ok: false,
            data: 0,
            err_msg: Some(msg),
        }
    }
}

#[wasm_bindgen]
pub struct I32Result {
    pub ok: bool,
    pub data: i32,
    err_msg: Option<String>,
}

#[wasm_bindgen]
impl I32Result {
    pub fn get_message(self) -> Option<String> {
        self.err_msg
    }
    pub fn get(&self) -> i32 {
        self.data
    }
    fn success(data: i32) -> Self {
        Self {
            ok: true,
            data,
            err_msg: None,
        }
    }
    fn failed(msg: String) -> Self {
        Self {
            ok: false,
            data: 0,
            err_msg: Some(msg),
        }
    }
}

#[wasm_bindgen]
pub struct StringListResult {
    pub ok: bool,
    data: Vec<String>,
    err_msg: Option<String>,
}

#[wasm_bindgen]
impl StringListResult {
    pub fn get_message(self) -> Option<String> {
        self.err_msg
    }
    pub fn length(&self) -> usize {
        self.data.len()
    }
    pub fn get(&self, i: usize) -> Option<String> {
        self.data.get(i).cloned()
    }
    fn success(data: Vec<String>) -> Self {
        Self {
            ok: true,
            data,
            err_msg: None,
        }
    }
    fn failed(msg: String) -> Self {
        Self {
            ok: false,
            data: vec![],
            err_msg: Some(msg),
        }
    }
}

#[wasm_bindgen]
pub struct ValueListResult {
    pub ok: bool,
    data: Vec<Value>,
    err_msg: Option<String>,
}

#[wasm_bindgen]
impl ValueListResult {
    pub fn get_message(self) -> Option<String> {
        self.err_msg
    }
    pub fn length(&self) -> usize {
        self.data.len()
    }
    pub fn get(&self, i: usize) -> Option<Value> {
        self.data.get(i).cloned()
    }
    fn success(data: Vec<Value>) -> Self {
        Self {
            ok: true,
            data,
            err_msg: None,
        }
    }
    fn failed(msg: String) -> Self {
        Self {
            ok: false,
            data: vec![],
            err_msg: Some(msg),
        }
    }
}
